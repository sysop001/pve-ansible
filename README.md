# PVE-ansible

Пакет для управления виртуальными машинами в среде Proxmox.

Возможности:
- создание виртуальной машины с заданными параметрами
- запуск / останов виртуальной машины
- удаление виртуальной машины
- создание / удаление / откат снапшотов
- создание файла для ssh для подключения к машинам.
- использование vault для хранения паролей

## Подготовка
- Создание темплейта виртуальной машины (проверено на debian 10/11, ubuntu 18)
- Создание пользователя для работы с API Proxmox
```bash 
# pveum useradd iac@pve
# pveum passwd iac@pve
# pveum roleadd iac-role -privs "VM.Config.Disk VM.Config.CPU VM.Config.Memory Datastore.AllocateSpace Sys.Modify VM.Config.Options VM.Allocate VM.Audit VM.Console VM.Config.CDROM VM.Config.Network VM.PowerMgmt VM.Config.HWType VM.Monitor VM.Clone"
# pveum aclmod / -user iac@pve -role iac-role
```
- Создание vault файла
```bash 
$ ansible-vault create vault.yml
ansible_ssh_pass: superp@ss
api_user: iac@pve
api_password: superp@ss
```
## Применение
### Описание тегов
- stop - останов виртуальных машин из списка
- remove - удаление виртуальных машин из списка
- cr-snap - создание снапшота виртуальной машины формата "snap_vmid"
- rm-snap - удаление снапшота виртуальной машины
- rb-snap - откат снапшота виртуальной машины

Прим: поддерживается только 1 снапшот, имя снапшота "захардкожено"

### Примеры запуска команд
```bash 
$ ansible-playbook -i hosts vm-manager.yml --extra-vars "@infra/vms-prod.yml" --ask-vault-pass
$ ansible-playbook -i hosts vm-manager.yml --extra-vars "@infra/vms-prod.yml" --ask-vault-pass --tags "stop,remove"
$ ansible-playbook -i hosts vm-manager.yml --extra-vars "@infra/vms-prod.yml" --ask-vault-pass --tags "cr-snap"
```
## TODO

- [ ] проверка использование VMID при создании виртуальной машины
- [ ] создание инвенторя для ansible
- [ ] переработать процедуру работы с публичным ключем пользователя: upload-apply-remove (не держать pubkey на PVE)

